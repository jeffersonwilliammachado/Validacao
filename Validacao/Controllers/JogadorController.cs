﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Validacao.Models;

namespace Validacao.Controllers
{
    public class JogadorController : Controller
    {
        public ActionResult Lista()
        {
            ValidacaoContext ctx = new ValidacaoContext();

            return View(ctx.Jogadores);
        }

        [HttpGet]
        public ActionResult Cadastra()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Cadastra(Jogador j)
        {
            if (ModelState.IsValid)
            {
                ValidacaoContext ctx = new ValidacaoContext();

                ctx.Jogadores.Add(j);
                ctx.SaveChanges();

                return RedirectToAction("Lista");
            }
            else
            {
                return View("Cadastra", j);
            }
        }
    }
}
